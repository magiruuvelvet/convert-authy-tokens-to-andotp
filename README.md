# Convert Authy tokens to andOTP

### Getting Started

**Requirements:**

 - Access to your Authy tokens JSON. The simpliest way is to own a rooted Android device and just copy the 2 files from `/data/data/com.authy.authy` to your PC:
   - `shared_prefs/com.authy.storage.tokens.authenticator.xml`
   - `shared_prefs/com.authy.storage.tokens.authy.xml`

The script was written on top of the Authy token JSON and most likely will not work with other formats. You can still manually write down the tokens or modify this script.

**As hint:** Authy's TOTP tokens should be `decryptedSecret` which are already valid tokens ready to use in other apps; and Authy's native tokens should be `secretSeed` which are HEX-encoded and must be converted to Base-32 (RFC 4648) first.

After you copied the XML files extract the JSON strings (`<string name="..."></string>`) and replace all occurrences of `&quot;` with `"` and save it to a new JSON file.

##### `com.authy.storage.tokens.authenticator.xml`

Contains TOTP tokens with 6 digits and a period of 30 seconds. Save the exported JSON to `exported/tokens.json`.

##### `com.authy.storage.tokens.authy.xml`

Contains native Authy tokens with usually 7 digits and a period of 10 seconds. Save the exported JSON to `exported/authy_tokens.json`.

<br><br>

Once this is done you can start with the convert process.

<br>

### Converting Tokens

You need Node.js to execute the convert script. I choose to use Node.js, because it natively supports JSON out of the box.

```sh
node convert.js
```

If you did everything right you now should see a file called `converted_tokens.json` in the root of this repository. This file is ready to import into andOTP using its import functionality. All tokens will have the default image. This process needs to be done manually in the andOTP app. After adding images it is recommended to backup the tokens again with their images added.

**Please be aware:** All token will work normally in andOTP, but I recommend you to keep Authy installed for some time until you verified all tokens to function correctly. Specially the Authy native tokens may cause some problems. For me they worked, but it doesn't mean they need to work for you too.

<br>

### Bonus Section: Token Properties

#### TOTP

 - Already valid (`decryptedSecret`)
 - Period: 30 seconds
 - 6 digits

#### Authy Native Tokens

 - HEX-encoded, must be converted to Base-32 (RFC 4648) (`secretSeed`)
 - Period: 10 seconds
 - usually 7 digits

<br>

This should give you some hints to understand how Authy's tokens work.
