const fs = require("fs");
const util = require("util");

const base32 = require("./hi-base32/base32.min.js")

const otp_schema = require("./otp_schema.json");

// exported from: com.authy.authy/shared_prefs/com.authy.storage.tokens.authenticator.xml
//   > normal TOTP codes according to the specification (base32)
//   > 6 digits, 30 seconds
//   > HTML-escaped JSON string
const authy_tokens = require("./exported/tokens.json");

// exported from: com.authy.authy/shared_prefs/com.authy.storage.tokens.authy.xml
//   > native Authy tokens with custom modifications, but TOTP compatible
//   > convert from HEX to base-32 (RFC 4648)
//   > 7 digits, 10 seconds
//   > HTML-escaped JSON string
const authy_native_tokens = require("./exported/authy_tokens.json");

// Array to store andOTP tokens
let otp_tokens = []

// Convert Authy TOTP tokens
for (const token of authy_tokens)
{
    // Authy            --> andOTP
    //--------------------------------------
    // decryptedSecret  --> secret
    // digits           --> digits
    // name             --> label

    converted = otp_schema;
    converted["secret"] = token["decryptedSecret"];
    converted["digits"] = token["digits"];
    converted["label"]  = token["name"];
    otp_tokens.push(JSON.stringify(converted));
}

// Convert Authy native tokens
for (const token of authy_native_tokens)
{
    // Authy            --> andOTP
    //--------------------------------------
    // secretSeed       --> secret
    // digits           --> digits
    // name             --> label
    // period: 10

    converted = otp_schema;
    converted["secret"] = base32.encode(new Buffer(token["secretSeed"], "hex"));
    converted["secret"] = converted["secret"].substring(0, converted["secret"].length - 6);
    converted["digits"] = token["digits"];
    converted["label"]  = token["name"];
    converted["period"] = 10;
    otp_tokens.push(JSON.stringify(converted));
}

fs.writeFileSync("./converted_tokens.json",  '[' + otp_tokens.join(',') + ']', "utf-8");
